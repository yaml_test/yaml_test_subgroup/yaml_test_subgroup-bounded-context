swagger: '2.0'
info:
    description: 'Locus Address Search Api - API Documentation'
    version: '1.0'
    title: 'Locus Address Search Lookup Api'
    termsOfService: 'http://terms-of-services.url'
    license:
        name: 'Apache License Version 2.0'
host: locus-es-address-lookup-api-uat.cfappsawsnpeast.ebiz.verizon.com
basePath: /
tags:
    -
        name: address-lookup-controller
        description: 'Address Lookup Controller'
paths:
    /addresses/street:
        get:
            tags:
                - address-lookup-controller
            summary: street
            description: 'This Locus Service search the street by the partial street name, the method takes care of fuzzy matching while searching the street such as Road, RD, Street, ST, Bouleward, BLWD etc .The most common use case of this api is to provide ''Type-Ahead'' search functionality. This Locus Service will return street address in a format <Street Number> <Street Name> <City> <State> <ZIp>. Ex. 140 RIVERSIDE DR N, BRICK, NJ, 08724 '
            operationId: getStreetByStreetTermUsingGET
            consumes:
                - application/json
            produces:
                - application/json
            parameters:
                -
                    name: streetterm
                    in: query
                    description: 'Partial street name such as "11 Liberty Ave"'
                    required: false
                    type: string
            responses:
                '200':
                    description: Success
                    schema:
                        $ref: '#/definitions/AddressLookupJSONResponse'
                '401':
                    description: Unauthorized
                '403':
                    description: Forbidden
                '404':
                    description: 'Not Found'
                '500':
                    description: 'Internal Server Error '
        post:
            tags:
                - address-lookup-controller
            summary: street
            description: 'This Locus Service search the street by the partial street name, the method takes care of fuzzy matching while searching the street such as Road, RD, Street, ST, Bouleward, BLWD etc .The most common use case of this api is to provide ''Type-Ahead'' search functionality. This Locus Service will return street address in a format <Street Number> <Street Name> <City> <State> <ZIp>. Ex. 140 RIVERSIDE DR N, BRICK, NJ, 08724 '
            operationId: getStreetByStreetTermUsingPOST
            consumes:
                - application/json
            produces:
                - application/json
            parameters:
                -
                    name: streetterm
                    in: query
                    description: 'Partial street name such as "11 Liberty Ave"'
                    required: false
                    type: string
            responses:
                '200':
                    description: Success
                    schema:
                        $ref: '#/definitions/AddressLookupJSONResponse'
                '401':
                    description: Unauthorized
                '403':
                    description: Forbidden
                '404':
                    description: 'Not Found'
                '500':
                    description: 'Internal Server Error '
    /addresses/streetelk:
        get:
            tags:
                - address-lookup-controller
            summary: streetelk
            description: "This Locus Service search the street by the partial street name, the method takes care of fuzzy matching while searching the street such as Road, RD, Street, ST, Boulevard, Blvd etc .The most common use case of this api is to provide 'Type-Ahead' search functionality. This Locus Service will return the raw address in a format <Street Number> <Street Name> <Unit Name/Number> <Floor> <City> <State> <Zip>. \n Ex. 140 RIVERSIDE DR APT 8K FLR 8, MANHATTAN, NY, 10024"
            operationId: getStreetByStreetTermParseUsingGET
            consumes:
                - application/json
            produces:
                - application/json
            parameters:
                -
                    name: logquery
                    in: query
                    description: logquery
                    required: false
                    type: string
                -
                    name: streetterm
                    in: query
                    description: 'Partial street name such as "11 Liberty Ave"'
                    required: false
                    type: string
            responses:
                '200':
                    description: Success
                    schema:
                        $ref: '#/definitions/AddressLookupJSONResponse'
                '401':
                    description: Unauthorized
                '403':
                    description: Forbidden
                '404':
                    description: 'Not Found'
                '500':
                    description: 'Internal Server Error '
    /addresses/units:
        get:
            tags:
                - address-lookup-controller
            summary: street
            description: 'This Locus Service returns list of units for given LocusID. An optional parameter ''unitterm'' helps to filter units matching with partial unnit name.'
            operationId: unitsUsingGET
            consumes:
                - application/json
            produces:
                - application/json
            parameters:
                -
                    name: locusId
                    in: query
                    description: 'Id, that identifies if given address is MDU for single unit. Ex 4567567'
                    required: false
                    type: string
                -
                    name: unitterm
                    in: query
                    description: 'Partial unit name or number. Ex APT1, APT2'
                    required: false
                    type: string
            responses:
                '200':
                    description: Success
                    schema:
                        $ref: '#/definitions/UnitDetailsResponseEnvelope'
                '401':
                    description: Unauthorized
                '403':
                    description: Forbidden
                '404':
                    description: 'Not Found'
                '500':
                    description: 'Internal Server Error '
        post:
            tags:
                - address-lookup-controller
            summary: street
            description: 'This Locus Service returns list of units for given LocusID. An optional parameter ''unitterm'' helps to filter units matching with partial unnit name.'
            operationId: unitsUsingPOST
            consumes:
                - application/json
            produces:
                - application/json
            parameters:
                -
                    name: locusId
                    in: query
                    description: 'Id, that identifies if given address is MDU for single unit. Ex 4567567'
                    required: false
                    type: string
                -
                    name: unitterm
                    in: query
                    description: 'Partial unit name or number. Ex APT1, APT2'
                    required: false
                    type: string
            responses:
                '200':
                    description: Success
                    schema:
                        $ref: '#/definitions/UnitDetailsResponseEnvelope'
                '401':
                    description: Unauthorized
                '403':
                    description: Forbidden
                '404':
                    description: 'Not Found'
                '500':
                    description: 'Internal Server Error '
    /addresses/zip:
        get:
            tags:
                - address-lookup-controller
            summary: zip
            description: 'This method search the zip based on partiaL zipcode or city name, the most common use case of this api is to provide ''Type-Ahead'' search functionality while locating zipcode'
            operationId: getZipByZipOrCityUsingGET
            consumes:
                - application/json
            produces:
                - application/json
            parameters:
                -
                    name: zipcityterm
                    in: query
                    description: 'Partial zipcode or city name. Ex. 076 or Mayw'
                    required: false
                    type: string
            responses:
                '200':
                    description: Success
                    schema:
                        $ref: '#/definitions/ZipJSONResponse'
                '401':
                    description: Unauthorized
                '403':
                    description: Forbidden
                '404':
                    description: 'Not Found'
                '500':
                    description: 'Internal Server Error '
definitions:
    Address:
        type: object
        properties:
            baseAddID:
                type: string
                description: 'The Id that identifies, if given address is MDU or single unit. Ex. 87654567'
                readOnly: true
            displayUnit:
                type: string
            disprawstr:
                type: string
                description: 'Name of the street with street number, city and zipcode. Ex. "401 HALF MOON BAY DR, CROTON ON HUDSON, NY, 10520"'
                readOnly: true
            dispstr:
                type: string
                description: 'Name of the street with street number. Ex. "LODESTONE WAY"  '
                readOnly: true
            elevvalue:
                type: string
                description: 'Level of the floor where unit is located. Ex. FLR1, FLR2. '
                readOnly: true
            elkStreet:
                type: string
                description: 'Name of the street Ex. "Moon Way Drive"'
                readOnly: true
            houseNumber:
                type: string
                description: 'Door Number. Ex. 126'
                readOnly: true
            locusID:
                type: string
                description: 'The Id that identifies each locus address uniquely. Ex. 45678654'
                readOnly: true
            locusLocation:
                type: string
                description: 'Navigational information of Locus Address - (Latitude, Longitude) Ex. (34.156104,-118.808391)'
                readOnly: true
            muni:
                type: string
                description: 'Name of the municipality. Ex.LONG BEACH'
                readOnly: true
            ntasAddrID:
                type: string
                description: 'The Id that identifies each nats address for the locus id. Ex. 45678654'
                readOnly: true
            postalcity:
                type: string
                description: 'Name of the city'
                readOnly: true
            state:
                type: string
                description: 'Name of the State. Ex. NY, NJ, MD'
                readOnly: true
            street:
                type: string
                description: 'Name of the street. Ex "Moon Way Drive"'
                readOnly: true
            unitvalue:
                type: string
                description: 'Unit Number. Ex. APT 704'
                readOnly: true
            zip:
                type: string
                description: 'Five digit zipcode. Ex. 10024'
                readOnly: true
    AddressBAU:
        type: object
        properties:
            baseAddID:
                type: string
                description: 'The Id that identifies, if given address is MDU or single unit. Ex. 87654567'
                readOnly: true
            city:
                type: string
                description: city
                readOnly: true
            elkStreet:
                type: string
                description: 'Name of the street Ex. "Moon Way Drive"'
                readOnly: true
            locusID:
                type: string
                description: 'The Id that identifies each address uniquely. Ex. 45678654'
                readOnly: true
            state:
                type: string
                description: state
                readOnly: true
            street:
                type: string
                description: 'Name of the street. Ex "Moon Way Drive"'
                readOnly: true
            unit:
                type: string
                description: unit
                readOnly: true
    AddressInfo:
        type: object
        properties:
            addressLine1:
                type: string
                description: 'Address Line 1  Ex. "Moon Way Drive"'
                readOnly: true
            addressLine2:
                type: string
                description: 'Address Line 2 Ex. "Apt 401"'
                readOnly: true
            addressid:
                type: string
                description: 'The Id that identifies each address uniquely. Ex. 45678654'
                readOnly: true
            city:
                type: string
                description: city
                readOnly: true
            state:
                type: string
                description: state
                readOnly: true
            zipCode:
                type: string
                description: zipCode
                readOnly: true
    AddressLookupJSONResponse:
        type: object
        properties:
            addresses:
                type: array
                items:
                    $ref: '#/definitions/Address'
            addressesbau:
                type: array
                items:
                    $ref: '#/definitions/AddressBAU'
            meta:
                $ref: '#/definitions/Meta'
            postValues:
                $ref: '#/definitions/Postvalues'
            qualification:
                $ref: '#/definitions/Qualification'
    Config:
        type: object
        properties:
            addressInfo:
                description: config
                readOnly: true
                $ref: '#/definitions/AddressInfo'
    Data:
        type: object
        properties:
            total:
                type: string
                description: 'Represents count of the records returned in the resultset.'
                readOnly: true
            unitDetails:
                type: array
                items:
                    $ref: '#/definitions/UnitDetails'
    Meta:
        type: object
        properties:
            code:
                type: string
                description: ' Represents Response Code of the request'
                readOnly: true
            description:
                type: string
                description: 'Description of the ''Response Code'''
                readOnly: true
            timestamp:
                type: string
                description: 'Represents date and time when request was executed'
                readOnly: true
    Postvalues:
        type: object
        properties:
            campaignCode:
                type: string
                description: campaignCode
                readOnly: true
            config:
                description: config
                readOnly: true
                $ref: '#/definitions/Config'
            vendorName:
                type: string
                description: 'vendor posting the details'
                readOnly: true
    Qualification:
        type: object
        properties:
            fiosqualified:
                type: boolean
                example: false
                description: 'check if the address is qualified for fios'
                readOnly: true
            gigqualified:
                type: boolean
                example: false
                description: 'check if the address is qualified for gigabit'
                readOnly: true
            posturl:
                type: string
                description: posturl
                readOnly: true
    UnitDetails:
        type: object
        properties:
            locusID:
                type: string
                description: 'The locusId that identifies each address uniquely. Ex. 45678654'
                readOnly: true
            unit:
                type: string
                description: 'Name/Number of the unit, that identifies given unit.'
                readOnly: true
    UnitDetailsResponseEnvelope:
        type: object
        properties:
            data:
                $ref: '#/definitions/Data'
            meta:
                $ref: '#/definitions/Meta'
    Zip:
        type: object
        properties:
            altCity:
                type: string
                description: 'Name of the AltCity'
                readOnly: true
            city:
                type: string
                description: 'Name of the City'
                readOnly: true
            state:
                type: string
                description: 'Name of the State'
                readOnly: true
            zip:
                type: string
                description: 'Represents 5 digit zipcode'
                readOnly: true
    ZipJSONResponse:
        type: object
        properties:
            meta:
                $ref: '#/definitions/Meta'
            zips:
                type: array
                items:
                    $ref: '#/definitions/Zip'
